---
title: Python for Data Science
subtitle: Python modules
date: 2019-08-15
tags: ["python", "code", "data science"]
---

How to import the main libraries used on DataScience

<!--more-->

* The statsmodels  library:

Statistical computations

https://pypi.org/project/statsmodels/

```python code for importing:
    import statsmodels as sm
```


* The seaborn  library:

Seaborn help us to create statistical graphics in python

https://pypi.org/project/seaborn/

```python code for importing:
    import seaborn as sns
```

* The numpy  library:

numpy is the tool for working with arrays

https://pypi.org/project/numpy/

```python code for importing:
    import statsmodels as sm
```

****

* The pandas  module:

pandas help us to load files as Dataframes(tabular data)
works with tabular data as in spreadsheets or database tables.

https://pypi.org/project/pandas/

```python code for importing:
    import python as pd
```

Pandas useful code:

```python code for reading a csv file as the df variable
df = pd.read_csv('file.csv')
```
